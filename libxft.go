// Copyright 2024 The libXft-go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:generate go run generator.go

// Package libXft is a ccgo/v4 version of libXft.a, the X FreeType interface
// library.
package libxft // import "modernc.org/libXft"
